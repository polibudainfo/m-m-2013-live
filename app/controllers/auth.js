//var mongoose = require('mongoose'),
//  Post = mongoose.model('Post');
//
//exports.index = function(req, res){
//  if(!req.loggedIn)
//  {
//    req.io.route('/login');
//  } else {
//    Post.find(function(err, articles){
//      if(err) throw new Error(err);
//
//      res.render('admin/index', {
//        title: 'Generator-Express MVC',
//        articles: articles
//      });
//    });
//  }
//};

var everyauth = require('everyauth');

everyauth.debug = true;

exports.isLogged = function(req, res){
  if(!req.loggedIn)
  {
    res.redirect(401, '/login');
  } else {
    res.redirect('/admin');
  }
}

exports.login = function(req, res){
  res.send('login');
}

exports.logout = function(req, res){
  res.send('logout');
}