var mongoose = require('mongoose'),
  Post = mongoose.model('Post');

exports.index = function(req, res){
  Post.find(function(err, articles){
    if(err) throw new Error(err);
    res.render('home/index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
};