module.exports = function(app){

	//home route
	var home = require('../app/controllers/home');
	app.get('/', home.index);

  //auth routes
  var auth = require('../app/controllers/auth');
  app.get('/login', auth.login);
  app.get('/logout', auth.logout);
  app.all('/admin*', auth.isLogged);

  //admin route
  var admin = require('../app/controllers/admin');
  app.get('/admin/', admin.index);

  var user = require('../app/controllers/user');
  app.get('/admin/user', user.list);
  app.post('/admin/user', user.add);
  app.get('/admin/user/:id', user.show);
  app.get('/admin/user/:id/edit', user.edit);

};
