var express = require('express.io')
  , everyauth = require('everyauth')
  , consolidate = require('consolidate');

module.exports = function(app, config) {
  app.configure(function () {
    app.engine('html', consolidate.swig)
    app.use(express.compress());
    app.use(express.static(config.root + '/public'));
    app.set('port', config.port);
    app.set('views', config.root + '/app/views');
    app.set('view engine', 'html');
    app.set('view cache', false);
    app.use(express.favicon(config.root + '/public/img/favicon.png'));
    app.use(express.logger('dev'));
    app.use(express.urlencoded());
    app.use(express.json());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(function(req, res) {
      res.status(404).render('404', { title: '404' });
    });
    app.use(everyauth.middleware(app));
  });
};
