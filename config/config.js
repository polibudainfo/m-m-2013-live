var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'miski2013'
    },
    port: 3000,
    db: 'mongodb://localhost/miski2013-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'miski2013'
    },
    port: 3000,
    db: 'mongodb://localhost/miski2013-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'miski2013'
    },
    port: 3000,
    db: 'mongodb://localhost/miski2013-production'
  }
};

module.exports = config[env];
