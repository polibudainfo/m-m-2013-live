# Misski 2013 - moduł relacji live strony wyborów Miss i Mistera internautów Politechniki Warszawskiej 2013

## Przygotowanie projektu

Do uruchomienia projektu potrzebne będzie ci Git i Node.js
By go zainstalować użyj [narzędzia odpowiedniego dla Twojego systemu operacyjnego](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager#ubuntu-mint)

## Elementy projektu

### Kod źródłowy

#### JS

Javascript jest zestrukturyzowany zgodnie z generatorem [Yeoman](http://yeoman.io/) dla [Express](http://expressjs.com/).
[Projekt generatora](https://github.com/petecoop/generator-express) i [jego Readme](https://github.com/petecoop/generator-express/blob/master/README.md) można znaleźć na GitHubie

### Grunt

Projekt używa narzedzia [Grunt](http://gruntjs.com/) do uruchamiania zadań CLI. Wszystkie zadania zdefiniowane są w
pliku Gruntfile.js w głównym folderze projektu

### Livereload

Jedną z komend Grunt'a jest ``livereload`` pozwala on na automatyczne przeładowanie projektu po jakiejkolwiek zmianie.
Po uruchomieniu ``grunt`` i zmianie któregokolwiek z plików projekt zostanie automatycznie zbudowany od nowa.

## Zarządzanie zależnościami

Do pobierania zależności projekt używa narzędzia [Bower](https://github.com/bower/bower).
Zależności są definiowane w bower.json, by je aktualizować nalezy zmienić numer wersji i uruchomić

    bower install

Bez tego nowododane pakiety nie będą załadowane do produkcji i testów


# Licencja

The MIT License (MIT)

Copyright (c) 2013 Rafał Kuć

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.